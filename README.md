# Build Blockchain HyperLedger Fabric Sample Application

## 1. Installation Guide
### a. Docker
**Docker version 17.03.0-ce or greater is required.**

#### Linux(Ubuntu)
First of all, in order to install docker correctly we need to install its dependencies:
```
sudo apt install apt-transport-https ca-certificates curl software-properties-common
```
Once the dependencies are installed, we can install docker:
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && \
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && \
sudo apt update && \
sudo apt install -y docker-ce
```
Now we need to manage th e current user in order to avoid using administration rights access whenever we use docker command(avoid typing sudo all the times). To do so, we need to add the current user to the docker group:

```
sudo groupadd docker ; \
sudo gpasswd -a ${USER} docker && \
sudo service docker restart
```
Do not mind if `groupadd: group 'docker' already exists` error pop up.

To apply the changes made, you need to logout/login. You can then check your version with:

```
docker -v
```
![](assets/README-514b2ae2.png)

#### Mac OS X
Download and install the latest [`Docker.dmg`](https://docs.docker.com/docker-for-mac/install/) package for Mac OS X available on the [Docker](https://docs.docker.com/docker-for-mac/install/) website. This will install `docker-compose` as well, so you can skip the next step.


#### Linux (not Ubuntu)

See links below:
- [Debian](https://docs.docker.com/engine/installation/linux/docker-ce/debian/)
- [Fedora](https://docs.docker.com/engine/installation/linux/docker-ce/fedora/)
- [CentOS](https://docs.docker.com/engine/installation/linux/docker-ce/centos/)

#### Windows

See instructions from the Docker website: [docker.com/docker-for-windows](https://docs.docker.com/docker-for-windows/install/)

### b. Docker Compose
**Docker-compose version 1.8 or grater is required.**
Why Docker-compose?
it is not easy to manage multiple containers at once.To solve this issue, we need **docker-compose**.

#### Linux(Ubuntu)
Docker compose Installation
```
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose && \
sudo chmod +x /usr/local/bin/docker-compose
```
to apply changes, logout and login and check version of docker compose

```
docker-compose version
```
![](assets/README-c7ce2063.png)
#### Windows / Others

See instructions from the Docker-compose website: [docs.docker.com/compose/install](https://docs.docker.com/compose/install/)

### c. Install Golang
**Go version 1.9.x or greater is required.**

#### Linux(Ubuntu)
You can either follow instructions from [golang.org](https://golang.org/dl/) or use those generics commands that will install Golang 1.9.2 and prepare your environment (generate your `GOPATH`) for Ubuntu:

```
wget https://storage.googleapis.com/golang/go1.9.2.linux-amd64.tar.gz && \
sudo tar -C /usr/local -xzf go1.9.2.linux-amd64.tar.gz && \
rm go1.9.2.linux-amd64.tar.gz && \
echo 'export PATH=$PATH:/usr/local/go/bin' | sudo tee -a /etc/profile && \
echo 'export GOPATH=$HOME/go' | tee -a $HOME/.bashrc && \
echo 'export PATH=$PATH:$GOROOT/bin:$GOPATH/bin' | tee -a $HOME/.bashrc && \
mkdir -p $HOME/go/{src,pkg,bin}
```
To make sure that the installation works, logout and login and run the command
```
go version
```
![](assets/README-25db6f9c.png)
#### Windows / Mac OS X / Others

See instructions from the Golang website: [golang.org/install](https://golang.org/doc/install)

## 4. Make your first Blockchain network

### a. Prepare environment

Before doing anything let's create our working directory:

```
mkdir -p $GOPATH/src/github.com/chainHero/heroes-service-network
```
### b. Dependencies

Download the HyperLedger Fabric Binary file

Go to official URL:  
* Version 1.2 for now : [https://hyperledger-fabric.readthedocs.io/en/release-1.2/install.html]
* You can download the binary file anywhere you want

* Once you have downloaded you will see a folder called fabric-samples:

![](assets/README-7cfd397b.png)

* go into that folder and copy `bin` folder to your project directory

### c. Configuration
#### a. Crypto-config

To get started we need to define what our network will be composed of. To do so , we need a configuration file.

It will contains the network topology and allows us to generate a set of certificates and keys for both Organization and the components that belong to those Organizations.

Let's create and start editing it

```
touch crpto-config.yaml
```
below is the crypto-config.yaml file and the explanation are inside

```
# "OrdererOrgs" - Definition of organizations managing orderer nodes
OrdererOrgs:
  - Name: chainHero
    Domain: hf.chainhero.io
    # Specs is an array of Spec entries.  Each Spec entry consists of two fields : Hostname and CommonName
    Specs:
      - Hostname: orderer
  - Name: dbnisHost
    Domain: edu.dbnis.io
    # Specs is an array of Spec entries.  Each Spec entry consists of two fields : Hostname and CommonName
    Specs:
      - Hostname: orderer
# "PeerOrgs" - Definition of organizations managing peer nodes
PeerOrgs:
  - Name: Org1ChainHero
    Domain: org1.hf.chainhero.io
    # Allows for the definition of 1 or more hosts that are created sequentially
    # from a template. By default, this looks like "peer%d" from 0 to Count-1.
    # You may override the number of nodes (Count), the starting index (Start)
    # or the template used to construct the name (Hostname).
    Template:
      #number of peer
      Count: 2
      Start: 1
    Users:
       # The number of user accounts _in addition_ to Admin
      Count: 2

```

Now that our configuration file is set up we can use it to create the basis of our network. To do so, we use the **Cryptogen** tool to generate the cryptographic material for our various network entities.
**cryptogen** is an utility for generating HyperLedger Fabric key material, however it is mainly meant to be used for testing environment.

Let's now create a folder named `crypto-config` or it will create by default and use **cryptogen** to initialized our network:
```
./bin/cryptogen generate --config=./crypto-config.yaml
```
![](assets/README-59a7d600.png)

#### b. Artifacts
In order for us to finish the initialization of our blockchain we need to create:
* The orderer genesis block. it is intended as its name indicate,to correctly initialize Fabric orderers.
* a channel genesis block. the channel is a kind of private network inside the main network which our users will be able to use. This alows you to initialized Fabric's peers to join a channel
* An anchor peer. Its a peer node with an additional feature which allow all other peers to discover and communicate with it.

So let's create this by editing a configuration file name `configtx.yaml`:

```
touch configtx.yaml
```

edit your configtx.yaml file as follow
```yaml


################################################################################
#
#   SECTION: Organizations
#
#   - This section defines the different organizational identities which will
#   be referenced later in the configuration.
#
################################################################################
Organizations:
    - &ChainHero
        Name: ChainHero
        ID: hf.chainhero.io
        AdminPrincipal: Role.ADMIN
        MSPDir: crypto-config/ordererOrganizations/hf.chainhero.io/msp
    - &DbnisOrg
        Name: DbnisOrg
        ID: edu.dbnis.io
        AdminPrincipal: Role.ADMIN
        MSPDir: crypto-config/ordererOrganizations/edu.dbnis.io/msp
    - &Org1ChainHero
        Name: ChainheroOrganization1
        ID: org1.hf.chainhero.io
        AdminPrincipal: Role.ADMIN
        MSPDir: crypto-config/peerOrganizations/org1.hf.chainhero.io/msp
        AnchorPeers:
            - Host: peer1.org1.hf.chainhero.io
              Port: 7051

################################################################################
#
#   SECTION: Orderer
#
#   - This section defines the values to encode into a config transaction or
#   genesis block for orderer related parameters.
#
################################################################################
Orderer: &OrdererDefaults
    OrdererType: solo
    Addresses:
        - orderer.hf.chainhero.io:7050
    BatchTimeout: 5s
     # Batch Size: Controls the number of messages batched into a block.
    BatchSize:
        # Max Message Count: The maximum number of messages to permit in a batch.
        MaxMessageCount: 10
        # Absolute Max Bytes: The absolute maximum number of bytes allowed for
        # the serialized messages in a batch. If the "kafka" OrdererType is
        # selected, set 'message.max.bytes' and 'replica.fetch.max.bytes' on the
        # Kafka brokers to a value that is larger than this one.
        AbsoluteMaxBytes: 98 MB
        # Preferred Max Bytes: The preferred maximum number of bytes allowed for
        # the serialized messages in a batch. A message larger than the
        # preferred max bytes will result in a batch larger than preferred max
        # bytes.
        PreferredMaxBytes: 512 KB
    # Max Channels is the maximum number of channels to allow on the ordering
    # network. When set to 0, this implies no maximum number of channels.
    MaxChannels: 0

    # Organizations is the list of orgs which are defined as participants on
    # the orderer side of the network.
    Organizations:

    ################################################################################
#
#   SECTION: Application
#
#   - This section defines the values to encode into a config transaction or
#   genesis block for application related parameters.
#
################################################################################
Application: &ApplicationDefaults
    Organizations:
################################################################################
#
#   SECTION : Profile
#
#   - Different configuration profiles may be encoded here to be specified
#   as parameters to the configtxgen tool. The profiles which specify consortiums
#   are to be used for generating the orderer genesis block.  With the correct
#   consortium members defined in the orderer genesis block, channel creation
#   requests may be generated with only the org member names and a consortium name
#
################################################################################
# for create genesis block and channels
Profiles:
    ChainHeroGenesisBlock:
        Orderer:
            <<: *OrdererDefaults
            Organizations:
                - *ChainHero
                - *DbnisOrg
        Application:
            <<: *ApplicationDefaults
            Organizations:
                - *Org1ChainHero

        Consortium: SampleConsortium
        Consortiums:
            SampleConsortium:
                Organizations:
                    - *ChainHero
                    - *DbnisOrg
                    - *Org1ChainHero

```
Now our configtx.yaml file is ready,let's create the artifacts folder, which will contain the output files generated by the binary `configtxgen`:
```
mkdir artifacts
```
Then, let's use the provided binary and our configuration file to create our **genesis block**:

```
./bin/configtxgen -profile ChainHeroGenesisBlock -outputBlock ./artifacts/orderer.genesis.block
```

![](assets/README-a6f1010b.png)

Now that it have been created let's see how to create the **channel**:

```
./bin/configtxgen -profile ChainHeroGenesisBlock -outputCreateChannelTx ./artifacts/chainhero.channel.tx -channelID chainhero
```
![](assets/README-a010ac8c.png)

Finally, we need a peer node which is called **the anchor peer**:

```
./bin/configtxgen -profile ChainHeroGenesisBlock -outputAnchorPeersUpdate ./artifacts/org1.chainhero.anchors.tx -channelID chainhero -asOrg ChainheroOrganization1
```
![](assets/README-aef5f477.png)

#### c. Docker-Compose
Make sure your Docker-Compose has already been installed.
Next, create a file named `docker-compose.yaml`

** Be sure to replace your peer organization CA with the file name in crypto-config/peerOrganizations/yourOrganization/ca/xxx
 **
 whenever you re-generate your crypto-config file you have to edit it.

 However, as you might have seen there is 2 files located here (the certificate and the key), the name of the file you need to copy should be something like that (finish with `_sk`):

example:
```
943fcd5dd60bfc47beeaa0a4736f75c0b0acbe13efb95cb5444e83cf12e0521b_sk
```
#### d. Launch your network
To run your HyperLedger Fabric Network run the following command
```
docker-compose up -d
```
** make sure all the services in docker-compose file is up and running
![](assets/README-af4b1244.png)

To check the running container :
```
docker ps
```

![](assets/README-4e06505b.png)
To check log of individual container you can you command:
```
docker logs container_name
```

# Hyperledger Fabric Go SDK

## 1. Use the Fabric Go SDK
### a. Configuration
Our application needs a lot of parameters, especially the addresses of the Fabric’s components to communicate. We will put everything in a new configuration file, the Fabric SDK Go configuration and our custom parameters. For the moment, we will only try to make the Fabric SDK Go works with the default chaincode:

create a new configuration file:

```
vi config.yaml
```
### b. Initialise
add a new folder named `blockchain` that will contain the whole interface that communicate with the network. We will see only the Fabric SDK Go in this folder:

```
mkdir blockchain
```
add a new file named ```setup.go```

add put the below content
```
package blockchain

import (
	"fmt"

	mspclient "github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
	"github.com/pkg/errors"
)

// FabricSetup implementation
type FabricSetup struct {
	ConfigFile      string
	OrgID           string
	OrdererID       string
	ChannelID       string
	ChainCodeID     string
	initialized     bool
	ChannelConfig   string
	ChaincodeGoPath string
	ChaincodePath   string
	OrgAdmin        string
	OrgName         string
	UserName        string
	admin           *resmgmt.Client
	sdk             *fabsdk.FabricSDK
}

// Initialize reads the configuration file and sets up the client, chain and event hub
func (setup *FabricSetup) Initialize() error {

	// Add parameters for the initialization
	if setup.initialized {
		return errors.New("sdk already initialized")
	}

	// Initialize the SDK with the configuration file
	sdk, err := fabsdk.New(config.FromFile(setup.ConfigFile))
	if err != nil {
		return errors.WithMessage(err, "failed to create SDK")
	}
	setup.sdk = sdk
	fmt.Println("SDK created")

	// The resource management client is responsible for managing channels (create/update channel)
	resourceManagerClientContext := setup.sdk.Context(fabsdk.WithUser(setup.OrgAdmin), fabsdk.WithOrg(setup.OrgName))
	if err != nil {
		return errors.WithMessage(err, "failed to load Admin identity")
	}
	resMgmtClient, err := resmgmt.New(resourceManagerClientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create channel management client from Admin identity")
	}
	setup.admin = resMgmtClient
	fmt.Println("Ressource management client created")

	// The MSP client allow us to retrieve user information from their identity, like its signing identity which we will need to save the channel
	mspClient, err := mspclient.New(sdk.Context(), mspclient.WithOrg(setup.OrgName))
	if err != nil {
		return errors.WithMessage(err, "failed to create MSP client")
	}
	adminIdentity, err := mspClient.GetSigningIdentity(setup.OrgAdmin)
	if err != nil {
		return errors.WithMessage(err, "failed to get admin signing identity")
	}
	req := resmgmt.SaveChannelRequest{ChannelID: setup.ChannelID, ChannelConfigPath: setup.ChannelConfig, SigningIdentities: []msp.SigningIdentity{adminIdentity}}
	txID, err := setup.admin.SaveChannel(req, resmgmt.WithOrdererEndpoint(setup.OrdererID))
	if err != nil || txID.TransactionID == "" {
		return errors.WithMessage(err, "failed to save channel")
	}
	fmt.Println("Channel created")

	// Make admin user join the previously created channel
	if err = setup.admin.JoinChannel(setup.ChannelID, resmgmt.WithRetry(retry.DefaultResMgmtOpts), resmgmt.WithOrdererEndpoint(setup.OrdererID)); err != nil {
		return errors.WithMessage(err, "failed to make admin join channel")
	}
	fmt.Println("Channel joined")

	fmt.Println("Initialization Successful")
	setup.initialized = true
	return nil
}

func (setup *FabricSetup) CloseSDK() {
	setup.sdk.Close()
}

```

this file is used for initialize our Fabric go sdk

At this stage, we only initialized a client that will communicate to a peer, a CA and an orderer. We also made a new channel and connected this peer to this channel. See the comments in the code for more information.



### c. Test
create `main.go` file
to make sure that client managed to initialize all the components. make a simple test with network launched. so we build the go code.

```
package main

import (
	"fmt"
	"os"

	"github.com/chainHero/heroes-service-network/blockchain"
)

func main() {
	// Definition of the Fabric SDK properties
	fSetup := blockchain.FabricSetup{
		// Network parameters
		OrdererID: "orderer.hf.chainhero.io",

		// Channel parameters
		ChannelID:     "chainhero",
		ChannelConfig: os.Getenv("GOPATH") + "/src/github.com/chainHero/heroes-service-network/artifacts/chainhero.channel.tx",

		// Chaincode parameters
		ChainCodeID:     "heroes-service-network",
		ChaincodeGoPath: os.Getenv("GOPATH"),
		ChaincodePath:   "github.com/chainHero/heroes-service-network/chaincode/",
		OrgAdmin:        "Admin",
		OrgName:         "org1",
		ConfigFile:      "config.yaml",

		// User parameters
		UserName: "User1",
	}

	// Initialization of the Fabric SDK from the previously set properties
	err := fSetup.Initialize()
	if err != nil {
		fmt.Printf("Unable to initialize the Fabric SDK: %v\n", err)
		return
	}
	// Close SDK
	defer fSetup.CloseSDK()
}

```
As you can see, we fixed the GOPATH of the environment if it’s not set. We will need this feature in order to compile the chaincode (we will see this in the next step).

The last thing to do, before starting the compilation, is to use a vendor directory that will contain all our dependencies. In our GOPATH we have Fabric SDK Go and maybe other projects. When we will try to compile our app, Golang search dependencies in our GOPATH, but first it checks if there is a vendor folder in the project. If the dependency is satisfied, then Golang doesn’t go looking in GOPATH or GOROOT. This is very useful when using several different versions of a dependency (some conflicts can happen, like multiple definitions of BCCSP in our case. We will handle this by using a tool like dep to flatten these dependencies in the vendor directory.


using dep
Create a file called Gopkg.toml and copy this inside:

inside `Gopkg.toml`

```
ignored = ["github.com/chainHero/heroes-service/chaincode"]

[[constraint]]
  # Release v1.0.0-alpha4
  name = "github.com/hyperledger/fabric-sdk-go"
  revision = "a906355f73d060d7bf95874a9e90dc17589edbb3"
  ```
if you haven't had dep let's install :
`go get github.com/golang/dep/cmd/dep`
And then add $GOPATH/bin to $PATH.
`export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin`

This is a constraint for dep in order to specify that in our vendor we want the SDK Go to a specific version.

run `dep ensure` to check for dependencies, if no dependencies exist it will automatically download

after dep ensure success you can compile your go code
run `go build`

after some time, a new binary file will appear in your project root directory. and we execute the binary by the following
`./heroes-service-network`

before that you need to start your docker-composer first.
`docker-compose up -d`

Note: if you see Initialization Successful you are good to go further.

### Clean up and Makefile
The Fabric SDK generates some files, like certificates, binaries and temporally files. Shutting down the network won’t fully clean up your environment and when you will need to start it again, these files will be reused to avoid building process. For development you can keep them to test quickly but for a real test, you need to clean up all and start from the beginning.

How clean up my environment ?
* Shut down your network: cd $GOPATH/src/github.com/chainHero/heroes-service-network && docker-compose down
* Remove credential stores (defined in the config file, in the client.credentialStore section): rm -rf /tmp/heroes-service-network-*

* Remove some docker containers and docker images not generated by the docker-compose command: docker rm -f -v `docker ps -a --no-trunc | grep "heroes-service-network" | cut -d ' ' -f 1` 2>/dev/null and docker rmi `docker images --no-trunc | grep "heroes-service-network" | cut -d ' ' -f 1` 2>/dev/null


How to be more efficient ?

We can automatize all these tasks in one single step. Also the build and start process can be automated. To do so, we will create a Makefile. First, ensure that you have the tool:

`make --version`
If make is not installed do (Ubuntu):

```
sudo apt install make
```
Then create a file named Makefile at the root of the project with this content:
```
.PHONY: all dev clean build env-up env-down run

all: clean build env-up run

dev: build run

##### BUILD
build:
	@echo "Build ..."
	@dep ensure
	@go build
	@echo "Build done"

##### ENV
env-up:
	@echo "Start environment ..."
	@docker-compose up --force-recreate -d
	@echo "Environment up"

env-down:
	@echo "Stop environment ..."
	@docker-compose down
	@echo "Environment down"

##### RUN
run:
	@echo "Start app ..."
	@./heroes-service-network

##### CLEAN
clean: env-down
	@echo "Clean up ..."
	@rm -rf /tmp/heroes-service-network-* heroes-service-network
	@docker rm -f -v `docker ps -a --no-trunc | grep "heroes-service-network" | cut -d ' ' -f 1` 2>/dev/null || true
	@docker rmi `docker images --no-trunc | grep "heroes-service-network" | cut -d ' ' -f 1` 2>/dev/null || true
	@echo "Clean up done"



```

Now with the task all:
1. the whole environment will be cleaned up,
2. then our go program will be compiled,
3. after which the network will be deployed and
4. finally the app will be up and running.

To use it, go in the root of the project and use the make command:
* Task all: make or make all
* Task clean: clean up everything and put down the network (make clean)
* Task build: just build the application (make build)
* Task env-up: just make the network up (make env-up)

### e. Install & instantiate the chaincode
We're almost there to use blockchain system. But now we haven't setup any chain code yet that will handle query from our application.
First, let's create a new directory to store chaincode named `chaincode` and add a new file named `main.go`

the following is the file definition:
```
package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

// HeroServiceNetwork implementation of chaincode
type HeroServiceNetworkChaincode struct {
}

// Init of the chaincode
// This function is called only one when the chaincode is instantiated.
// So the goal is to prepare the ledger to handle future requests.
func (t *HeroServiceNetworkChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("=====HeroServiceNetwork Chain code init======")

	//TODO: get the function and arguments from the request
	function, _ := stub.GetFunctionAndParameters()
	// Check if the request is the init function
	if function != "init" {
		return shim.Error("Unknown function called")
	}

	// Put in the ledger the key/value hello/world
	err := stub.PutState("hello", []byte("world"))
	if err != nil {
		return shim.Error(err.Error())
	}

	// Return successful message
	return shim.Success(nil)
}

// TODO: INVOKE
// All future request named invoke will arrive here
func (t *HeroServiceNetworkChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("########### HeroesServiceChaincode Invoke ###########")
	// Get the function and argument from the request
	function, args := stub.GetFunctionAndParameters()

	// Check if it is an invoke request
	if function != "invoke" {
		return shim.Error("Unknown function called")
	}

	// Check if the number of arguments is sufficient
	if len(args) < 1 {
		return shim.Error("The number of arguments is insufficient")
	}

	// In order to manage multiple type of request , we will check the first argument.
	// Here we have one possible arguemnt: query(every query request will read in the ledger without modification)

	if args[0] == "query" {
		return t.query(stub, args)
	}

	return shim.Error("Unknow action, check the first argument")
}

// TODO : QUERY
// Every readonly functions in the ledger will be here
func (t *HeroServiceNetworkChaincode) query(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("########### HeroesServiceChaincode query ###########")

	// Check whether the number of arguments is sufficient
	if len(args) < 2 {
		return shim.Error("The amount of arguemnts is insufficient")
	}

	// Like the Invoke function, we manage multiple type of query requests with the second argument.
	// We also have only one possible argument: hello

	if args[1] == "hello" {
		// Get the state of the value matching the key hello in the ledger

		state, err := stub.GetState("hello")
		if err != nil {
			return shim.Error("Failed to get state ")
		}

		return shim.Success(state)
	}

	return shim.Error(" Unknown query action, check the second argument")
}

func main() {
	// Start the chaincode and make it ready for futures requests
	err := shim.Start(new(HeroServiceNetworkChaincode))
	if err != nil {
		fmt.Printf("Error starting Heroes Service chaincode: %s", err)
	}
}

```

**Note**:
the chaincode itself doesn't really related to the application. we can have one repository for the app and one for the chaincode.

For now, the chaincode is only put the key/value `hello/world` in the ledger at initialization. in addition, there is one funcation that we can call by an invoke: `query hello`. This function get the state of the ledger, i.e `hello` and give it in response. we will test this in next step after successfully install and instantiate the chaincode.
In order to install and instantiate chaincode , we need to add some code in the application by edit our `blockchain/setup.go` with the following:
```
[...]
func (setup *FabricSetup) InstallAndInstantiateCC() error {

	// Create the chaincode package that will be sent to the peers
	ccPkg, err := packager.NewCCPackage(setup.ChaincodePath, setup.ChaincodeGoPath)
	if err != nil {
		return errors.WithMessage(err, "failed to create chaincode package")
	}
	fmt.Println("ccPkg created")

	// Install example cc to org peers
	installCCReq := resmgmt.InstallCCRequest{Name: setup.ChainCodeID, Path: setup.ChaincodePath, Version: "0", Package: ccPkg}
	_, err = setup.admin.InstallCC(installCCReq, resmgmt.WithRetry(retry.DefaultResMgmtOpts))
	if err != nil {
		return errors.WithMessage(err, "failed to install chaincode")
	}
	fmt.Println("Chaincode installed")

	// Set up chaincode policy
	ccPolicy := cauthdsl.SignedByAnyMember([]string{"org1.hf.chainhero.io"})

	resp, err := setup.admin.InstantiateCC(setup.ChannelID, resmgmt.InstantiateCCRequest{Name: setup.ChainCodeID, Path: setup.ChaincodeGoPath, Version: "0", Args: [][]byte{[]byte("init")}, Policy: ccPolicy})
	if err != nil || resp.TransactionID == "" {
		return errors.WithMessage(err, "failed to instantiate the chaincode")
	}
	fmt.Println("Chaincode instantiated")

	// Channel client is used to query and execute transactions
	clientContext := setup.sdk.ChannelContext(setup.ChannelID, fabsdk.WithUser(setup.UserName))
	setup.client, err = channel.New(clientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create new channel client")
	}
	fmt.Println("Channel client created")

	// Creation of the client which will enables access to our channel events
	setup.event, err = event.New(clientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create new event client")
	}
	fmt.Println("Event client created")

	fmt.Println("Chaincode Installation & Instantiation Successful")
	return nil
}
[...]
```
**Tips**:
Take care of the chaincode version, if you want to update your chaincode, increment the version number of this setup.go file.Otherwise, the network will keep the same chaincode.

We need to modify our `main.go` file in order to call our new function.
```
[...]
// Install and instantiate the chaincode
	err = fSetup.InstallAndInstantiateCC()
	if err != nil {
		fmt.Printf("Unable to install and instantiate the chaincode: %v\n", err)
		return
	}
[...]
```

We can test this, just with the make command setup in the previous step:
```
make
```
**Tips:**
 the installation and the instantiation don’t need to be run at every start of the application. Only when we update the chaincode (and the chaincode version). A solution is to provide an argument when we run the application to tell to do this additional procedure before move on. Since in this tutorial we will clean up the environment every time we don’t really care about that.

 ### f. Query the chaincode
Like a database, the chaincode is plugged and ready to answer. Let's try the hello query.
We will put all query functions in a new file named `query.go` inside `blockchain` folder

`query.go`:
```
package blockchain

import (
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
)

// QueryHello query the chaincode to get the state of hello
func (setup *FabricSetup) QueryHello() (string, error) {
	// Prepare arguments
	var args []string
	args = append(args, "invoke")
	args = append(args, "query")
	args = append(args, "hello")

	response, err := setup.client.Query(channel.Request{ChaincodeID: setup.ChainCodeID,
		Fcn:  args[0],
		Args: [][]byte{[]byte(args[1]), []byte(args[2])}})
	if err != nil {
		return "", fmt.Errorf("failed to query: %v", err)

	}

	return string(response.Payload), nil

}

```
Then you can add the call to this new function in `main.go`
```
func main(){
  [...]

	// Query the chaincode
	response, err := fSetup.QueryHello()
	if err != nil {
		fmt.Printf("Unable to query hello on the chaincode: %v\n", err)
	} else {
		fmt.Printf("Response from the query hello: %s\n", response)
	}
}
```

Let's test by run the `make` command again and the result should be:
![](assets/README-b2590e8a.png)
