package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

// HeroServiceNetwork implementation of chaincode
type HeroServiceNetworkChaincode struct {
}

// Init of the chaincode
// This function is called only one when the chaincode is instantiated.
// So the goal is to prepare the ledger to handle future requests.
func (t *HeroServiceNetworkChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("=====HeroServiceNetwork Chain code init======")

	//TODO: get the function and arguments from the request
	function, _ := stub.GetFunctionAndParameters()
	// Check if the request is the init function
	if function != "init" {
		return shim.Error("Unknown function called")
	}

	// Put in the ledger the key/value hello/world
	err := stub.PutState("hello", []byte("world"))
	if err != nil {
		return shim.Error(err.Error())
	}

	// Return successful message
	return shim.Success(nil)
}

// TODO: INVOKE
// All future request named invoke will arrive here
func (t *HeroServiceNetworkChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("########### HeroesServiceChaincode Invoke ###########")
	// Get the function and argument from the request
	function, args := stub.GetFunctionAndParameters()

	// Check if it is an invoke request
	if function != "invoke" {
		return shim.Error("Unknown function called")
	}

	// Check if the number of arguments is sufficient
	if len(args) < 1 {
		return shim.Error("The number of arguments is insufficient")
	}

	// In order to manage multiple type of request , we will check the first argument.
	// Here we have one possible arguemnt: query(every query request will read in the ledger without modification)

	if args[0] == "query" {
		return t.query(stub, args)
	}

	return shim.Error("Unknow action, check the first argument")
}

// TODO : QUERY
// Every readonly functions in the ledger will be here
func (t *HeroServiceNetworkChaincode) query(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("########### HeroesServiceChaincode query ###########")

	// Check whether the number of arguments is sufficient
	if len(args) < 2 {
		return shim.Error("The amount of arguemnts is insufficient")
	}

	// Like the Invoke function, we manage multiple type of query requests with the second argument.
	// We also have only one possible argument: hello

	if args[1] == "hello" {
		// Get the state of the value matching the key hello in the ledger

		state, err := stub.GetState("hello")
		if err != nil {
			return shim.Error("Failed to get state ")
		}

		return shim.Success(state)
	}

	return shim.Error(" Unknown query action, check the second argument")
}

func main() {
	// Start the chaincode and make it ready for futures requests
	err := shim.Start(new(HeroServiceNetworkChaincode))
	if err != nil {
		fmt.Printf("Error starting Heroes Service chaincode: %s", err)
	}
}
